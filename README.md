# Sequence® Connector Registry

A list of all the available connectors is available [here](https://gitlab.com/reductech/sequence/connector-registry/-/packages).

For pre-release and development versions, please see the
[Connector Registry Dev](https://gitlab.com/reductech/sequence/connector-registry-dev).

For more information on how to use connectors in Sequence, please see the
[documentation](https://sequence.sh/docs/quick-start#connectors) or the
[Connector Manager](https://gitlab.com/reductech/sequence/connectormanager).
